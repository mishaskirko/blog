<?php

use App\Http\Controllers\Auth\LoginUserController;
use App\Http\Controllers\Auth\RegisterUserController;
use App\Http\Controllers\Homepage\HomepageController;
use App\Http\Controllers\Newsletter\NewsletterController;
use App\Http\Controllers\Post\PostCommentsController;
use App\Http\Controllers\Post\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(HomepageController::class)->group(function () {
    Route::get('/', 'index')->name('homepage.index');
});

Route::controller(RegisterUserController::class)->group(function () {
    Route::get('register', 'show')->middleware('guest')->name('register.show');
    Route::post('register', 'register')->middleware('guest')->name('register');
});

Route::controller(LoginUserController::class)->group(function () {
    Route::get('login', 'show')->middleware('guest')->name('login.show');
    Route::post('login', 'login')->middleware('guest')->name('login');
    Route::post('logout', 'logout')->middleware('auth')->name('logout');
});

Route::controller(PostController::class)->middleware(['admin', 'auth'])->group(function () {
    Route::resource('post', PostController::class)->except('show');
});

Route::controller(PostController::class)->group(function () {
    Route::get('post/{post:slug}', 'show')->name('post.show');
});

Route::controller(PostCommentsController::class)->group(function () {
    Route::post('posts/{post:slug}/comments', 'store')->name('posts.comments.store');
});

Route::controller(NewsletterController::class)->group(function () {
    Route::post('email-subscription', 'createEmailSubscription')->middleware('auth')
        ->name('email-subscription.store');
});
