<?php

namespace App\Interfaces;

interface NewsletterInterface
{
    /**
     * @param string $email
     * @param string|null $list
     */
    public function subscribe(string $email, string $list = null);
}
