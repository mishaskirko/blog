<?php

namespace App\Services;

use App\Interfaces\NewsletterInterface;
use MailchimpMarketing\ApiClient;

class MailchimpService implements NewsletterInterface
{
    /**
     * @param ApiClient $client
     */
    public function __construct(
        protected ApiClient $client
    ) {}

    /**
     * @param string $email
     * @param string|null $list
     * @return mixed
     */
    public function subscribe(string $email, string $list = null): mixed
    {
        $list ??= config('services.mailchimp.lists.subscribers');
        return $this->client->lists->addListMember($list, [
            'email_address' => $email,
            'status' => 'subscribed',
        ]);
    }
}
