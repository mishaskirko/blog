<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class HomepageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('homepage.index', [
            'posts' => Post::latest()
                ->filter(
                    request(
                        [
                            'search',
                            'category',
                            'author'
                        ]
                    )
                )
                ->paginate(6)
                ->withQueryString()
        ]);
    }
}
