<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\Comment\CreateCommentRequest;
use App\Models\Post;
use Exception;

class PostCommentsController extends Controller
{
    public function store(Post $post, CreateCommentRequest $request)
    {
        try {
            $validated = $request->validated();
            $validated['user_id'] = $request->user()->id;
            $post->comments()->create($validated);
            return back();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
