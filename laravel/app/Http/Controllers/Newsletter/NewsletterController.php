<?php

namespace App\Http\Controllers\Newsletter;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmailSubscriptionRequest;
use App\Interfaces\NewsletterInterface;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;

class NewsletterController extends Controller
{
    /**
     * @param EmailSubscriptionRequest $request
     * @param NewsletterInterface $mailchimp
     * @return Application|RedirectResponse|Redirector
     */
    public function createEmailSubscription(
        EmailSubscriptionRequest $request,
        NewsletterInterface      $mailchimp
    ): Application|RedirectResponse|Redirector
    {
        try {
            $validated = $request->validated();
            $mailchimp->subscribe($validated['email']);
        } catch (Exception $e) {
            throw ValidationException::withMessages([
                'email' => $e->getMessage(),
            ]);
        }

        return redirect('/')->with('success', 'Thanks for subscribing!');
    }
}
