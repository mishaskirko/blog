<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Models\User;
use Auth;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class RegisterUserController extends Controller
{
    public function show(): Factory|View|Application
    {
        return view('register.show');
    }

    public function register(RegisterUserRequest $request, User $user): string|Redirector|RedirectResponse|Application
    {
        try {
            Auth::login($user->create($request->validated()));
            return redirect('/')->with('success', 'Your account has been created.');
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
