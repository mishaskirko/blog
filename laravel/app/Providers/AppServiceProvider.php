<?php

namespace App\Providers;

use App\Interfaces\NewsletterInterface;
use App\Services\MailchimpService;
use Illuminate\Support\ServiceProvider;
use MailchimpMarketing\ApiClient;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(
            NewsletterInterface::class, function () {
            $client = (new ApiClient)->setConfig([
                'apiKey' => config('services.mailchimp.api_key'),
                'server' => config('services.mailchimp.server_prefix'),
            ]);
            return new MailchimpService($client);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
