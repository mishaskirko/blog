<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $with = ['category', 'author', 'comments'];

    /**
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)->orderByDesc('created_at');
    }

    /**
     * @param Builder $query
     * @param array $filters
     * @return void
     */
    public function scopeFilter(Builder $query, array $filters): void
    {
        $query->when($filters['search'] ?? false,
            fn(Builder $query, string $search) => $query->where(
                fn(Builder $query) => $query->where('title', 'like', "%$search%")
                    ->orWhere('excerpt', 'like', "%$search%")
                    ->orWhere('body', 'like', "%$search%")
            ));

        $query->when($filters['category'] ?? false,
            fn(Builder $query, string $category) => $query->whereHas('category',
                fn(Builder $query) => $query->where('slug', $category)
            )
        );

        $query->when($filters['author'] ?? false,
            fn(Builder $query, string $author) => $query->whereHas('author',
                fn(Builder $query) => $query->where('username', $author)
            )
        );
    }
}
