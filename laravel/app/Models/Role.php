<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name'
    ];

    public const ADMIN = 'admin';
    public const USER = 'user';

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return Builder|Role
     */
    public static function getUserRole(): Builder|Role
    {
        return self::where('name', self::USER)->first();
    }

    /**
     * @return Builder|Role
     */
    public static function getAdminRole(): Builder|Role
    {
        return self::query()->where('name', self::ADMIN)->first();
    }

    /**
     * @param string $name
     * @return int
     */
    public static function getRoleIdByName(string $name): int
    {
        return self::where('name', $name)->first()->id;
    }

    /**
     * @return int
     */
    public static function getAdminRoleId(): int
    {
        return self::getRoleIdByName(self::ADMIN);
    }

    /**
     * @return int
     */
    public static function getUserRoleId(): int
    {
        return self::getRoleIdByName(self::USER);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->name === self::ADMIN;
    }

    /**
     * @return bool
     */
    public function isUser(): bool
    {
        return $this->name === self::USER;
    }
}
