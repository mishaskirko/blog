<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Comment::factory(5)->sequence(
            fn(Sequence $sequence) => [
                'user_id' => User::query()->inRandomOrder()->first()->id,
                'post_id' => Post::query()->inRandomOrder()->first()->id,
            ]
        )->create();
    }
}
