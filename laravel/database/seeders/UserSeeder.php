<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory(1)
            ->hasAttached(Role::getAdminRole(),
                [
                    'role_id' => Role::getAdminRoleId(),
                    'created_at' => now(),
                    'updated_at' => now(),
                ])
            ->create(
                [
                    'email' => 'admin@mail.com',
                    'password' => 'password',
                ]
            );

        User::factory(10)
            ->hasAttached(Role::getUserRole(),
                [
                    'role_id' => Role::getUserRoleId(),
                    'created_at' => now(),
                    'updated_at' => now(),
                ])
            ->create();
    }
}
