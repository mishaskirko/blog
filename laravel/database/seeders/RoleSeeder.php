<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Role::factory(2)
            ->state(
                new Sequence(
                    ['name' => Role::ADMIN],
                    ['name' => Role::USER]
                ))
            ->create();
    }
}
