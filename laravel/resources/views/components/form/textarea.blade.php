@props(['name', 'value' => '', 'required' => false, 'class' => 'border border-gray-200 p-2 w-full rounded'])
<x-form.field>
    <x-form.label name="{{$name}}"/>
    <textarea class="{{$class}}"
              name="{{$name}}"
              id="{{$name}}"
              required="{{$required}}"
            {{$attributes}}
    >
        {{ $slot ?? old($name) }}
    </textarea>
    <x-form.error name="{{$name}}"/>
</x-form.field>
