<button type="submit" {{$attributes(['class' => "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4
rounded-full mt-10 border border-blue-600 hover:border-blue-500"])}}
>
    {{ $slot }}
</button>
