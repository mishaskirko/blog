@props(['name', 'type' => 'text', 'autocomplete' => 'off', 'required' => false])

<x-form.field>
    <x-form.label name="{{$name}}"/>
    <input class="border border-gray-200 p-2 w-full rounded"
           type="{{$type}}"
           name="{{$name}}"
           id="{{$name}}"
           autocomplete="{{$autocomplete}}"
        {{$attributes(['value' => old($name), 'required' => $required])}}
    >
    <x-form.error name="{{$name}}"/>
</x-form.field>
