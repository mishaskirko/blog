@props(['name', 'labelName', 'options', 'value' => '', 'required' => false])
<x-form.field>
    <x-form.label name="{{$labelName}}"/>
    <select class="border border-gray-400 p-2 w-full"
            name="{{$name}}"
            id="{{$name}}"
            required="{{$required}}"
    >
        @foreach($options as $option)
            <option value="{{$option->id}}"
                {{$attributes(['selected' => $option->id == $value])}}
            >
                {{ucwords($option->name)}}
            </option>
        @endforeach
    </select>
    <x-form.error name="{{$name}}"/>
</x-form.field>
