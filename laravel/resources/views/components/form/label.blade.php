@props(['name'])

<label class="block mb-2 uppercase text-xs font-bold text-gray-700"
       for="{{$name}}"
>
    {{ucfirst($name)}}
</label>
