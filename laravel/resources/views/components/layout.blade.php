<!doctype html>

<title>Algochad Blog</title>
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@2.x.x/dist/alpine.min.js" defer></script>
<style>
    html {
        scroll-behavior: smooth;
    }
</style>
<body style="font-family: Open Sans, sans-serif">
<section class="px-6 py-8">
    <nav class="md:flex md:justify-between md:items-center">
        <div>
            <a href="{{route('homepage.index')}}">
                <img src="/images/logo.svg" alt="Laracasts Logo" width="165" height="16">
            </a>
        </div>
        <div class="mt-8 md:mt-0 flex items-center">
            @auth
                <x-drop-down>
                    <x-slot name="trigger">
                        <button class="text-xs font-bold uppercase">
                            Welcome, {{ auth()->user()->name }}
                        </button>
                    </x-slot>
                    @if(auth()->user()->isAdministrator())
                        <x-drop-down-item href="{{route('post.index')}}" :active="request()->is('posts')">
                            Dashboard
                        </x-drop-down-item>
                        <x-drop-down-item href="{{route('post.create')}}" :active="request()->is('post')">
                            New Post
                        </x-drop-down-item>
                    @endif
                    <x-drop-down-item
                        href="#"
                        x-data="{}"
                        @click.prevent="document.querySelector('#logout-form').submit()"
                    >
                        Logout
                    </x-drop-down-item>
                    <form id="logout-form" action="{{route('logout')}}" method="POST" class="hidden">
                        @csrf
                    </form>
                </x-drop-down>
            @else
                <a href="{{route('register.show')}}" class="text-xs font-bold uppercase">Register</a>
                <a href="{{route('login.show')}}" class="ml-3 text-xs font-bold uppercase">Login</a>
            @endauth
            <a href="#subscribe-form"
               class="bg-blue-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5">
                Subscribe for Updates
            </a>
        </div>
    </nav>
    {{ $slot }}
    <footer class="bg-gray-100 border border-black border-opacity-5 rounded-xl text-center py-16 px-10 mt-16">
        <img src="/images/chad-logo.png" alt="" class="mx-auto -mb-0" style="width: 145px;">
        <h5 class="text-3xl">Stay in touch with the latest posts</h5>
        <p class="text-sm mt-3">Promise to keep the inbox clean. No bugs.</p>
        <x-email-subscribe-form/>
    </footer>
</section>
<x-flash-success-register-message/>
</body>
