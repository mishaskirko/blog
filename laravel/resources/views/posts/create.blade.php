<x-layout>
    <x-setting heading="Publish New Post">
        <form method="POST" action="/post" enctype="multipart/form-data">
            @csrf
            <x-form.input name="title" required="true"/>
            <x-form.input name="slug" required="true"/>
            <x-form.input name="thumbnail" type="file" required="true"/>
            <x-form.textarea name="excerpt" required="true"/>
            <x-form.textarea name="body" required="true"/>
            <x-form.select name="category_id" :options="$categories" required="true" labelName="Category"/>
            <x-form.button>
                Publish
            </x-form.button>
        </form>
    </x-setting>
</x-layout>
