<x-layout>
    <x-setting :heading="'Edit Post: ' . $post->title">
        <form method="POST" action="{{route('post.update', $post->slug)}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <x-form.input name="title" required="true" :value="old('title', $post->title)"/>
            <x-form.input name="slug" required="true" :value="old('slug', $post->slug)"/>
            <div class="flex mt-6">
                <div class="flex-1">
                    <x-form.input name="thumbnail"
                                  type="file"
                                  :value="old('thumbnail', $post->thumbnail)"
                    />
                </div>
                <img src="{{asset('storage/' . $post->thumbnail)}}" alt="Thumbnail" class="rounded-xl" width="100">
            </div>
            <x-form.textarea name="excerpt" required="true">
                {{old('excerpt', $post->excerpt)}}
            </x-form.textarea>
            <x-form.textarea name="body" required="true">
                {{old('body', $post->body)}}
            </x-form.textarea>
            <x-form.select name="category_id"
                           :options="$categories"
                           required="true"
                           labelName="Category"
                           :value="old('category_id', $post->category->id)"
            />
            <x-form.button>
                Update
            </x-form.button>
        </form>
    </x-setting>
</x-layout>
