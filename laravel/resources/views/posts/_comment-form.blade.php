@auth()
    <x-panel>
        <form method="POST" action="{{route('posts.comments.store', $post->slug)}}">
            @csrf
            <header class="flex items-center">
                <img src="/images/chad-logo.png" alt="" width="50" height="50"
                     class="rounded-full">
                <h3 class="font-bold ml-4">
                    Want to share your thoughts?
                </h3>
            </header>
            <div class="flex items-center mt-6">
                <label for="body"></label>
                <textarea
                    name="body"
                    id="body"
                    rows="10"
                    cols="30"
                    class="w-full text-sm border border-gray-200 rounded-xl p-2 focus:outline-none
                                             focus:ring"
                    placeholder="Leave a comment"
                    required
                ></textarea>
                <input type="hidden" name="user_id" disabled value="{{auth()->user()->id}}">
            </div>
            @error('body')
            <span class="text-red-500 text-xs italic">{{ $message }}</span>
            @enderror
            <div class="flex justify-end">
                <x-form.button>
                    Post Comment
                </x-form.button>
            </div>
        </form>
    </x-panel>
@else
    <p class="font-semibold">
        <a href="{{route('register.show')}}" class="hover:underline">
            Register
        </a>
        or
        <a href="{{route('login.show')}}" class="hover:underline">
            Login
        </a>
        to leave a comment.
    </p>
@endauth
