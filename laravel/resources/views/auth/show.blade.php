<x-layout>
    <secttion class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10">
            <x-panel>
                <h1 class="text-xl font-bold text-center">Log In!</h1>
                <form method="POST" action="{{route('login')}}">
                    @csrf
                    <x-form.input name="email" type="email" required="true" autocomplete="username"/>
                    <x-form.input name="password" type="password" required="true" autocomplete="current-password"/>
                    <x-form.button>
                        Log In
                    </x-form.button>
                </form>
            </x-panel>
        </main>
    </secttion>
</x-layout>
