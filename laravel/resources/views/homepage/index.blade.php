<x-layout>
    @include('homepage._header')
    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
        @if($posts->count())
            <x-post-grid :posts="$posts" />
            {{ $posts->links() }}
        @else
            <div class="flex justify-center">
                <div class="text-center">
                    <h1 class="text-3xl font-semibold">No posts yet</h1>
                    <p class="text-gray-500">
                        Be the first to create a post!
                    </p>
                </div>
            </div>
        @endif
    </main>
</x-layout>
