<x-layout>
    <secttion class="px-6 py-8">
        <main class="max-w-lg mx-auto bg-gray-100 border border-gray-200 p-6 rounded-xl">
            <form method="POST" action="{{route('register')}}">
                @csrf
                <h1 class="text-xl font-bold text-center">Register!</h1>
                <x-form.input name="name" type="text" required="true" autocomplete="name"/>
                <x-form.input name="username" type="text" required="true" autocomplete="username"/>
                <x-form.input name="email" type="email" required="true" autocomplete="email"/>
                <x-form.input name="password" type="password" required="true" autocomplete="new-password"/>
                <x-form.button>
                    Register
                </x-form.button>
            </form>
        </main>
    </secttion>
</x-layout>
